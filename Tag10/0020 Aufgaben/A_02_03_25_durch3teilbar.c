/*

*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	system("chcp 1252");
	system("cls");
	int start, ende, zaehler=0;

	printf("\nDurch 3 teilbare Zahlen im Bereich von ...");
	printf("\nBitte Startwert eingeben: ");
	scanf("%d",&start);
	printf("Bitte Endwert eingeben: ");
	scanf("%d",&ende);
	printf("\nDurch 3 teilbare Zahlen im Bereich von %d bis %d sind:\n", start, ende);

	for(int i=start; i<=ende; i++)
	{
		if(i%3==0)
		{
			printf("%d ", i);
			zaehler++;
		}
		if(zaehler==5)
		{
			printf("\n");
			zaehler=0;
		}
	}
	
	printf("\n\n");
	return 0;
}


