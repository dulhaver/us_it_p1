/*
Sie sollen ein Programm f�r die IHK schreiben, dass als Eingabe die Punkte erh�lt und als Ausgabe die entsprechende Note erzeugt.
Die IHK stellt Ihnen folgendes Schema zur Verf�gung

Punkt		Note
100 - 92	1
91 - 81		2
80 - 67		3
66 - 50		4
49 - 30		5
29 - 0		6

Erstellen Sie zuerst einen PAP und anschlie�end das C Programm.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int punkte;
	
	system("chcp 1252");
	system("cls");
	
	printf("\nPunkte eingeben: ");
	scanf("%d", &punkte);	
	
	if(punkte>=92)			
	{
		printf("\nNote 1");
	}
	else if(punkte>=81)
		{
			printf("\nNote 2");
		}
		else if(punkte>=67)
			{
				printf("\nNote 3");
			}
			else if(punkte>=50)
				{
					printf("\nNote 4");
				}
				else if(punkte>=30)
					{
						printf("\nNote 5");
					}
					else
					{
						printf("\nNote 6");
					}
	
	printf("\n\n");
	system("pause");
	
	return 0;
}