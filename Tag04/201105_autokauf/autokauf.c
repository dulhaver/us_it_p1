# include <stdio.h>

int main()
{

  system("clear");

  float preisA = 15000;
  float rueckkaufA = 1500;
  float totalA = preisA - rueckkaufA;

  float leasingRateB = 250;
  float totalB = leasingRateB * 12 * 5;


  if (totalA < totalB)
  {
      printf("\nKaufangebot A ist mit Gesamtkosten von %.2f EUR besser als Leasingangebot B (%.2f)! \n", totalA, totalB);
  }

  else
  {
      printf("\nLeasingangebot B ist mit Gesamtkosten von %.2f EUR besser als Kaufangebot A (%.2f)! \n", totalB, totalA);
  }

      printf("-----------------------------------------------------------------------------\n");

  return 0;
}
