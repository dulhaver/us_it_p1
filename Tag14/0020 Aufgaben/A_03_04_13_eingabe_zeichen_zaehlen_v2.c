#include <stdio.h>
#include <stdlib.h>

int main()
{
	// system("chcp 1252");
	system("clear");

	// Variable vom Typ int f�r die Anzahl der Zeichen = Gr��e des arrays deklarieren
	int array_groesse;

	// Anzahl der Zeichen vom user abfragen und in Variable speichern
	// hier noch printf
	scanf("%d", &array_groesse);

	// array of char mit der Anzahl anlegen
	char zeichen[array_groesse];

	// die o.g. Anzahl der Zeichen abfragen und in das array eintragen
	// hier noch printf
	for(int i = 0; i < array_groesse; i++)
	{
		// fflush(stdin);
		scanf("%c", &zeichen[i]);
	}

	// wir brauchen eine Schleife, um durch alle Elemente des Arrays durchzugehen
	// Was passiert mit dieser ersten Schleife?
	// lies Zeichen bei Index 0  z.B. ein 'e'

	char zeichen_zu_zaehlen;
	int zaehler;
	// wir brauchen Z�hler als Variable

	for(int i = 0; i < array_groesse; i++)
	{

		// der Z�hler muss vor jeder inneren Schleife auf 0 zur�ckgesetzt werden
		zaehler=0;
		zeichen_zu_zaehlen = zeichen[i];

		// innere Schleife --> das gesamte array  wird durchlaufen
		for(int j = 0; j < array_groesse; j++)
		{
			// �berall, wo 'e' == array[i], dann Z�hler hochsetzen			// 'e' == 'e'
			if(zeichen_zu_zaehlen == zeichen[j])
			{
				zaehler++;
			}
		}

		// Ausgabe des Z�hlers zu dem aktuellen Zeichen
		printf("\nAnzahl dieses Zeichens: %d", zaehler);

	}

	// dann �u�ere Schleife weitersetzen

	printf("\n\n");

	return 0;
}
