/*
	Erstellen Sie eine Abfrage, die vom Nutzer die anzuzeigende Seite (mit Seiten-Nr.) abfragt. Lassen Sie �ber ein switch-Statement die gew�nschte Seite in Form einer Meldung auf dem Bildschirm anzeigen.
*/
#include <stdio.h>
#include <stdlib.h>
// #include <math.h>

#define FALL1 1		// symbolische Konstante
					// Name der Konstante mit Gro�buchstaben
					// Zuweisung eines Wertes ohne =
#define FALL2 2
#define FALL3 3

#define MEIN_PI		3.14159265358979323846

int main()
{
	system("chcp 1252");
	system("cls");

	int seitenZahl;

	printf("\nEingabe Seitenzahl: ");
	scanf("%d",&seitenZahl);

	switch (seitenZahl)
	{
		case 1: 	printf("\nSeite: %d",seitenZahl);
					break;
		case 2: 	printf("\nSeite: %d",seitenZahl);
					break;
		case 3: 	printf("\nSeite: %d",seitenZahl);
					break;
		case 4: 	printf("\nSeite: %d",seitenZahl);
					break;
		default: 	printf("\nSeite %d nicht gefunden.",seitenZahl);
					break;
	}

	// -----------------------------------------------------------------------
	/*
	int seite1=1, seite2=2, seite3=3, seite4=4;

	// dieses switch funktioniert nicht
	// case kann keine Variablen verarbeiten/vergleichen, auch keine int-Variablen
	switch (seitenZahl)
	{
		case seite1: printf("\nSeite: %d",seitenZahl); break;
		case seite2: printf("\nSeite: %d",seitenZahl); break;
		case seite3: printf("\nSeite: %d",seitenZahl); break;
		case seite4: printf("\nSeite: %d",seitenZahl); break;
		default: printf("\nSeite %d nicht gefunden.",seitenZahl); break;
	}
	*/

	// -----------------------------------------------------------------------

	char auswahlZeichen;
	printf("\n\nEingabe Zeichen: ");
	fflush(stdin);
	scanf("%c", &auswahlZeichen);

	switch (auswahlZeichen)
	{
		case 'a': printf("\nZeichen: %c", auswahlZeichen); break;
		case 'b': printf("\nZeichen: %c", auswahlZeichen); break;
		case 'c': printf("\nZeichen: %c", auswahlZeichen); break;
		case 'd': printf("\nZeichen: %c", auswahlZeichen); break;
		default: printf("\nZeichen %c nicht gefunden.", auswahlZeichen); break;
	}

	// -----------------------------------------------------------------------

	// das wiederum funktioniert

	printf("\n\n");
	seitenZahl=1;
	switch (seitenZahl)
	{
		case FALL1: printf("\nSeite: %d",seitenZahl); break;
		case FALL2: printf("\nSeite: %d",seitenZahl); break;
		case FALL3: printf("\nSeite: %d",seitenZahl); break;
		default: 	printf("\nSeite %d nicht gefunden.",seitenZahl); break;
	}

	printf("\n\n");
	return 0;
}
