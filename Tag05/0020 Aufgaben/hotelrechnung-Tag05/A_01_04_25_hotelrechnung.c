/* A U F G A B E
Errechnen Sie den Endbetrag einer Hotelrechnung und geben Sie den ermittelten Wert aus. Die Anzahl der �bernachtungen und der Preis pro �bernachtung sollen vom Benutzer eingegeben werden. Beachten Sie bei der Berechnung des Endbetrages, dass das Hotel 10% Erm��igung gibt, wenn der Hotelgast mehr als 10 �bernachtungen hat.

Erstellen Sie das Programm in Pseudocode.
Erstellen Sie den C-Quelltext. */

// -------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");

	int anz_uebernachtung;
	float pp_uebernachtung, gesamt_preis;

	printf("\nEingabe Anzahl Übernachtungen: ");
	fflush(stdin);
	scanf("%d",&anz_uebernachtung);

	printf("\nEingabe Preis pro Übernachtung: ");
	fflush(stdin);
	scanf("%f",&pp_uebernachtung);

	if(anz_uebernachtung>10)
	{
		gesamt_preis = anz_uebernachtung * pp_uebernachtung * 0.9;
	}

	else
	{
		gesamt_preis = anz_uebernachtung * pp_uebernachtung;
	}

	printf("\nDer Gesamtpreis ist %f Euros.",gesamt_preis);

	return 0;
}
