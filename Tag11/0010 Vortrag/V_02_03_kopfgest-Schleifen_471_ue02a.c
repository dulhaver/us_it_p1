/*
	ue02:
	Erstellen Sie ein Programm, das von einem Startwert bis zu einem Endwert r�ckw�rts z�hlt und die Werte auf dem Bildschirm ausgibt.
	Startwert und Endwert sollen vom User abgefragt werden. Es muss sichergestellt werden, dass der Startwert gr��er als der Endwert ist.
*/

#include <stdio.h>				
#include <windows.h>

int main()						
{
	SetConsoleOutputCP(1252);	
	SetConsoleCP(1252);			
	system("cls");				

	int startwert, endwert;
	int schleifeAusfuehren=1;	
	
	printf("\nBitte Startwert eingeben: ");		
	scanf("%d", &startwert);	
	
	while(schleifeAusfuehren)
	{
		printf("\nBitte Endwert eingeben, der kleiner ist als der Startwert: ");
		scanf("%d", &endwert);
		if(endwert<startwert)
		{
			schleifeAusfuehren=0;			
		}
	}
	
	while(startwert>=endwert)					
	{
		printf("\naktueller Wert ist : %d", startwert);
		startwert=startwert-1;
	}
	
	printf("\nEnde des Programms");
	
	// system("pause");
	printf("\n");
	return 0;
}