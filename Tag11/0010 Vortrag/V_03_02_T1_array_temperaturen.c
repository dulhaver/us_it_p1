/*

*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
	system("chcp 1252");
	system("cls");
	
	int temperaturen[7]={25,22,18,12,15,19,23};
	float temp_avg=0.;
	int temp_min, temp_min_tag, temp_max, temp_max_tag, temp_summe;
	int temp_diff=0, temp_diff_tag1=1, temp_diff_tag2=1;
	
	temp_min=temperaturen[0];			// hier werden einige Variablen initialisiert
	temp_min_tag=1;
	temp_max=temperaturen[0];
	temp_max_tag=1;
	temp_summe=temperaturen[0];
	
	for(int i=1; i<7;i++)
	{
		temp_summe=temp_summe+temperaturen[i];
		if(temperaturen[i]>temp_max)
		{
			temp_max=temperaturen[i];
			temp_max_tag=i+1;			// hier wird die Ordnungszahl des Tages gemerkt (und nicht der Index)
		}
		if(temperaturen[i]<temp_min)
		{
			temp_min=temperaturen[i];
			temp_min_tag=i+1;			// hier wird die Ordnungszahl des Tages gemerkt (und nicht der Index)
		}
		if(abs(temperaturen[i]-temperaturen[i-1]) > temp_diff)
		{
			temp_diff=abs(temperaturen[i]-temperaturen[i-1]);
			temp_diff_tag1=i-1;		// hier wird der Index des Vortages gemerkt
			temp_diff_tag2=i;		// hier wird der Index des Tages gemerkt 
		}
	}
	
	temp_avg=temp_summe/7.0;		// 7. um eine float-Division zu erzwingen
								// sonst int/int ergibt eine ganzzahlige int-Division
	printf("\nMittelwert: %f", temp_avg);
	printf("\nMax: %d am %d. Tag", temp_max, temp_max_tag);
	printf("\nMin: %d am %d. Tag", temp_min, temp_min_tag);
	printf("\nDie Differenz zwischen h�chstem und niedrigstem Wert ist: %d", temp_max-temp_min);
	printf("\nDifferenz zwischen aufeinanderfolgenden Tagen: %d zwischen %d. und %d. Tag.", temp_diff, temp_diff_tag1+1, temp_diff_tag2+1); // BEACHTE: Index + 1, z.B. der 3. Tag hat Index 2
	// die Wochentage sind von Montag = 1 bis Sonntag = 7 nummeriert 	
	
	printf("\n\n");
	return 0;
}


