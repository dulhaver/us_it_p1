#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");

  int i;
  char name[6] = {'W', 'e', 'b', 'e', 'r', 'n'};

  printf("\n------5 printf befehle--------------------------\n");

  printf("\n Der Name ist: %c", name[0]);
  printf("%c", name[1]);
  printf("%c", name[2]);
  printf("%c", name[3]);
  printf("%c\n", name[4]);

  printf("\n------single printf befehl--------------------------\n");

  printf("\n Der Name ist: %c%c%c%c%c\n", name[0], name[1], name[2], name[3], name[4]);

  printf("\n------loop-mit Webern-----------------\n");

  printf("\n Der Name ist: ");
  i=0;
  while (i<=5)
  {
    printf("%c", name[i]);
    i++;
  }

  printf("\n\n------geben Sie reWe aus--------------------------\n");

  printf("\n Der Name ist: %c%c%c%c\n", name[4], name[3], name[0], name[3]);


  printf("\n\n------geben Sie Rewe aus--------------------------\n");

  name[0] = 'w';  // ab hier fuer immer 'w'
  name[4] = 'R';  // ab hier fuer immer 'R'

  printf("\n Der Name ist: %c%c%c%c\n", name[4], name[3], name[0], name[3]);


  printf("\n----------------------------------------------\n");


   return 0;
}
