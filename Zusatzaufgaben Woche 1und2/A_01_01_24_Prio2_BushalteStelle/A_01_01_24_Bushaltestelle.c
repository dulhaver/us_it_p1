/*

*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
	system("chcp 1252");
	system("cls");
	
	int stunden, minuten, restMinuten;
	
	
	printf("\nEingabe Uhrzeit volle Stunden: ");
	fflush(stdin);
	scanf("%d",&stunden);

	printf("Eingabe Uhrzeit Minuten: ");
	fflush(stdin);
	scanf("%d",&minuten);
	
	printf("\nUhrzeit: %d:%02d",stunden, minuten);
	// %02d  --> zweistellige Ausgabe, ggf. mit f�hrender Null
	
	// 40%15=10 --> 15-10 = 5
	// 59%15=14 --> 15-14 = 1
	// 0%15=0 --> 15-0 = 15
	// 
	restMinuten=15-(minuten%15);
	printf("\nDer Bus f�hrt in %d Minuten.",restMinuten);
	
	printf("\n\n");
	return 0;
}


