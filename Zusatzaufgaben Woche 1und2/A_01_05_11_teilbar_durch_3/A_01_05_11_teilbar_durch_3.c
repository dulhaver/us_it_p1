/*
Erstellen Sie einen C Programm, das pr�ft, ob eine eingegebene Ganzzahl durch 3 ohne Rest teilbar ist und geben Sie die Information auf die Konsole aus.
*/
#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
		
	int zahl1;
	int weiter;

	// Im Folgenden sind unterschiedliche Varianten der Programmsteuerung realisiert.
	
	/*
	do
	{	
		printf("Bitte Zahl eingeben: ");
		scanf("%d",&zahl1);
	
		if((zahl1%3)==0)
			printf("Zahl ist restlos durch 3 teilbar.");
		else
			printf("Zahl ist NICHT restlos durch 3 teilbar.");
		
		printf("\nWeiter JA=1 NEIN=0\n");
		scanf("%d",&weiter);
	}
	while(weiter);
	*/
	
	/*
	do
	{	
		printf("Bitte Zahl eingeben: ");
		scanf("%d",&zahl1);
	
		if((zahl1%3)==0)
			printf("Zahl ist restlos durch 3 teilbar.");
		else
			printf("Zahl ist NICHT restlos durch 3 teilbar.");
		
		printf("\nWeiter JA=1 NEIN=0\n");
		scanf("%d",&weiter);
	}
	while(weiter==1);
	*/
	
	
	do
	{	
		printf("Bitte Zahl eingeben: ");
		scanf("%d",&zahl1);
	
		if((zahl1%3)==0)
			printf("Zahl ist restlos durch 3 teilbar.");
		else
			printf("Zahl ist NICHT restlos durch 3 teilbar.");
		
		printf("\nWeiter JA=1 NEIN=0\n");
		scanf("%d",&weiter);
		
		if(weiter==0) return 0;
		
	}
	while(1);
	
	return 0;
}