/*

*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	system("chcp 1252");
	system("cls");

	int zu_ratende_zahl, tipp, anz_versuche=1;
	
	printf("\nLustiges Zahlenraten");
	printf("\nZahl eingeben: ");
	scanf("%d", &zu_ratende_zahl);
	
	
	printf("\nTipp eingeben: ");
	scanf("%d", &tipp);
	// diese Eingabe k�nnte man auch vermeiden bei der Verwendung einer fu�gesteuerten Schleife
	
	while(zu_ratende_zahl!=tipp)
	{
		if(zu_ratende_zahl<tipp)
			printf("\nzu gro� --> ");
		else
			printf("\nzu klein --> ");
		
		printf("Tipp eingeben: ");
		scanf("%d", &tipp);
		anz_versuche++;
	}
	
	printf("\nRICHTIG. Sie haben %d Versuche ben�tigt.", anz_versuche);
	
	printf("\n\n");
	return 0;
}


