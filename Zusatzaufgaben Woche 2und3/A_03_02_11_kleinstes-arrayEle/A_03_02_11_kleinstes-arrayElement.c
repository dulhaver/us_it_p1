#include <stdio.h>
#include <stdlib.h>

int main()
{
	system("chcp 1252");
	system("cls");
	
	int array[5]={23,675,9,101,90};
	int kleinsteZahl, position=1;
	
	kleinsteZahl=array[0];
	
	for(int i=1; i<5; i++)
	{
		if(array[i]<kleinsteZahl)
		{
			kleinsteZahl=array[i];
			position=i+1;
		}
	}
	
	printf("\nDas kleinste Element ist die Zahl %d", kleinsteZahl);
	printf("\nDas kleinste Element ist das %d. Element im Array, hat also den Index %d.", position, (position-1));
	
	printf("\n\n");
	 
	return 0;
}
