#include <stdio.h>
#include <stdlib.h>

int main()

{
  system("clear");


  float qtyApple, qtyPear, litreAjuice, litrePjuice;
  float priceApple=0.3, pricePear=0.25, priceAjuice=1.03, pricePjuice=1.25;
  float netAppel, netPear, netAjuice, netPjuice;

  printf("\n Anzahl Aepfel: ");
  scanf("%f", &qtyApple);
  printf("\n Anzahl Birnen: ");
  scanf("%f", &qtyPear);
  printf("\n Liter A-Saft: ");
  scanf("%f", &litreAjuice);
  printf("\n Liter B-Saft: ");
  scanf("%f", &litrePjuice);

  netAppel = qtyApple * priceApple;
  netPear = qtyPear * pricePear;
  netAjuice = litreAjuice * priceAjuice;
  netPjuice = litrePjuice * pricePjuice;

  float netTotal = netAppel + netPear + netAjuice + netPjuice;
  float discountAppel = 0;
  float vatFruit = 0.07, vatJuice = 0.19;
  float grossTotal;

  if (netTotal>10 && qtyApple>=5) {
    discountAppel = netAppel * 0.05;
    netTotal = netTotal - discountAppel;
  }
  else
  {
    netTotal = netTotal;
  }
  grossTotal = netTotal + (netAppel+netPear) * vatFruit + (netAjuice+netPjuice)*vatJuice;

  printf("\n netAppel: %.2f, netPear: %.2f, netAjuice: %.2f, netPjuice: %.2f\n", netAppel, netPear, netAjuice, netPjuice);
  printf("\n netTotal:\t %5.2f", netTotal);
  printf("\n discountAppel: -\t %5.2f\n", discountAppel);
  printf("\n vat 7%%:\t %5.2f", (netAppel+netPear) * vatFruit);
  printf("\n vat 19%%:\t %5.2f", (netAjuice+netPjuice) * vatJuice);
  printf("\n ===================================================================");
  printf("\n grossTotal:\t %5.2f\n", grossTotal);

   return 0;
}
