/*Sie sollen berechnen, in wie vielen Jahren ein Kredit zur�ckgezahlt ist. Der Kredit wird durch eine festen H�he der zu zahlenden Rate �ber die gesamte Laufzeit getilgt. Die Rate setzt sich zusammen aus der Tilgung und den zu zahlenden Zinsen.  Geben Sie f�r jedes Jahr an, wie hoch der Kredit ist, wie hoch die Tilgung ist und wie viele Zinsen f�r das abgelaufene Jahr gezahlt wurden. Gehen Sie von einem Kreditbetrag von 10.000 �, einem Zinssatz von 3% und einer Rate von 500 � im Jahr aus.  
Beschreiben Sie die Vorgehensweise in Pseudocode und anschlie�end in C
*/

start_program kredit
	kredit=eingabe()
	zinssatz=eingabe()
	rate=eingabe()
	
	jahr=1
	
	solange(kredit>0)
		zinsen=kredit*zinssatz/100
		ausgabe(zinsen)
		tilgung=rate-zinsen	
		kredit=kredit-tilgung
		ausgabe(kredit)
		jahre=jahre+1
	end_solange

	ausgabe(jahre)

end_program