#include <stdio.h>
#include <stdlib.h>

int main()


// C program to check leap year using if else.
// C Program to input year from user and check whether the given year is leap year or not using ladder if else.
// A year a leap year, if the year is exactly divisible by 4 but and not divisible by 100.
// Year is also a leap year if it is exactly divisible by 400.


{
  system("clear");

  int year, isLeap;


  printf("\n Enter a year (4digits): ");
  scanf("\n%d", &year);


  if (year%4==0 && year%100!=0 || year%400==0)
  {
    printf("\n the year %d is a Leapyear", year);
  }
  else
  {
    printf("\n the year %d is a regular year", year);
  }

  printf("\n----------------------------------------------\n");


   return 0;
}
