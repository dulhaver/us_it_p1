#include <stdio.h>
#include <stdlib.h>

int main()


// Write a C program to input electricity unit charge and calculate the total electricity bill according to the given condition:
// For first 50 units Rs. 0.50/unit
// For next 100 units Rs. 0.75/unit
// For next 100 units Rs. 1.20/unit
// For unit above 250 Rs. 1.50/unit
// An additional surcharge of 20% is added to the bill.


{
  system("clear");

  int usage;
  float price50=0.5, price150=0.75, price250=1.2, priceplus=1.5;
  float net50=0, net150=0, net250=0, netplus=0;
  float netTotal;
  float surchargePercent=0.2, baseFee=33;
  float grossTotal;

  printf("\n input units last month: ");
  scanf("%d", &usage);

  if(usage<=50) {
    netTotal=usage*price50;
  } else  if(usage>50 && usage<=150) {
            netTotal=50*price50 + (usage-50)*price150;
          }  else  if(usage>150 && usage<=250) {
                    netTotal=50*price50 + 100*price150 + (usage-150)*price250;
                  }  else
                        netTotal=50*price50 + 100*price150 + 100*price250 + (usage-250+priceplus);


  // int surcharge = netTotal * surchargePercent;
  float surcharge = netTotal*surchargePercent;
  grossTotal = baseFee+netTotal+surcharge;


  printf("\n----------------------------------------------\n");

  printf("\n Usage last month: \t%9d units", usage);
  printf("\n net Total: \t\t%9.2f $", netTotal);
  printf("\n 20%% surcharge: \t%9.2f $", surcharge);
  printf("\n monthly base fee: \t%9.2f $", baseFee);
  printf("\n gross Total: \t\t%9.2f $", grossTotal);

  printf("\n----------------------------------------------\n");

   return 0;
}
